----------------------------------------------------------------------
--
-- ProcNaturalSemantics.hs
-- Programming Languages
-- Fall 2019
--
-- Natural Semantics for Proc
-- [Nielson and Nielson, semantics with applications]
--
-- Author: Alberto Carmona López, Sergio Cayuela Domínguez
----------------------------------------------------------------------

module ProcNaturalSemantics where

import           Proc

----------------------------------------------------------------------
-- Variable Declarations
----------------------------------------------------------------------

-- locations

type Loc = Integer

new :: Loc -> Loc
new l = l + 1

-- variable environment

type EnvVar = Var -> Loc

-- store

-- 'sto [next]' refers to the first available cell in the store 'sto'
next :: Loc
next = 0

type Store = Loc -> Z

-- | Exercise 1.1

-- update a variable environment with a new binding envV [x -> l]
updateV :: EnvVar -> Var -> Loc -> EnvVar
updateV envV x l = newEnvV
  where
    newEnvV var = if var == x then l else envV var

-- update a store with a new binding sto [l -> v]
updateS :: Store -> Loc -> Z -> Store
updateS sto l v = newSto
  where
    newSto loc = if loc == l then v else sto loc

-- variable declaration configurations

data ConfigD = InterD DecVar EnvVar Store  -- <Dv, envV, store>
             | FinalD EnvVar Store         -- <envV, store>

nsDecV :: ConfigD -> ConfigD

-- | Exercise 1.2

-- var x := a
nsDecV (InterD (Dec x a decs) envV store) = nsDecV (InterD decs newV newS)
  where nextLoc = store next
        newV = updateV envV x nextLoc
        intS = updateS store nextLoc (aVal a (store . envV))
        newS = updateS intS next (new nextLoc)

-- epsilon
nsDecV (InterD EndDec envV store)         = FinalD envV store

----------------------------------------------------------------------
-- Procedure Declarations
----------------------------------------------------------------------

-- procedure environment

--                    p    s    snapshots    previous
--                    |    |     /    \         |
data EnvProc = EnvP Pname Stm EnvVar EnvProc EnvProc
             | EmptyEnvProc

-- | Exercise 2.1

-- update the procedure environment
updP :: DecProc -> EnvVar -> EnvProc -> EnvProc
updP (Proc p s procs) envV envP = updP procs envV (EnvP p s envV envP envP)
updP EndProc envV envP          = envP

-- updP (Proc p s procs) envV envP 
-- updP procs@(Proc p' s' procs') envV (EnvP p s envV envP envP)
-- updP procs'(Proc p'' s'' procs'') envV (EnvP p' s' envV (EnvP p s envV envP envP) (EnvP p s envV envP envP))

-- | Exercise 2.2

-- lookup procedure p
envProc :: EnvProc -> Pname -> (Stm, EnvVar, EnvProc)
envProc (EnvP q s envV envP envs) p 
  | q == p                          = (s, envV, envP)
  | otherwise                       = envProc envs p
envProc EmptyEnvProc p              = error "undefined procedure p"

-- representation of configurations for Proc

data Config = Inter Stm Store  -- <S, sto>
            | Final Store      -- sto

-- representation of the transition relation <S, sto> -> stos'

nsStm :: EnvVar -> EnvProc -> Config -> Config

-- | Exercise 3.1

-- x := a

nsStm envV envP (Inter (Ass x a) sto)            = Final sto'
  where
    sto' = updateS sto (envV x) (aVal a s)
    s = sto . envV

-- skip

nsStm envV envP (Inter Skip sto)                 = Final sto


-- s1; s2

nsStm envV envP (Inter (Comp ss1 ss2) sto)       = config''
  where
    config'@(Inter stm sto')  = nsStm envV envP (Inter ss1 sto)
    config''                  = nsStm envV envP (Inter ss2 sto')

-- if b then s1 else s2

nsStm envV envP (Inter (If b s1 s2) sto)
  | bVal b s  = nsStm envV envP (Inter s1 sto)
  | otherwise = nsStm envV envP (Inter s2 sto)
    where
      s = sto . envV

-- while b do s

nsStm envV envP (Inter (While b s) sto)          = undefined

-- block vars procs s

nsStm envV envP (Inter (Block vars procs s) sto) = undefined

-- non-recursive procedure call
{-
nsStm envV envP (Inter (Call p) sto) = undefined


-}

-- recursive procedure call
nsStm envV envP (Inter (Call p) sto)             = undefined

-- | Exercise 3.2

sNs :: Stm -> Store -> Store
sNs s sto = undefined
