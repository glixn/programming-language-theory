{-|

Programming Languages
Fall 2019

Implementation in Haskell of the Natural Semantics described in Chapter 2 of
Nielson & Nielson, Semantics with Applications

Author: Alberto Carmona López, Sergio Cayuela Domínguez

-}

module Exercises02 where

import           Exercises01      (Update (..), fvAexp, fvBexp, update, aexp1, s1)
import           NaturalSemantics
import           Data.List
-- import           Test.HUnit       hiding (State)
import           While

-- |----------------------------------------------------------------------
-- | Exercise 1
-- |----------------------------------------------------------------------
-- | The function 'sNs' returns the final state of the execution of a
-- | WHILE statement 'st' from a given initial state 's'. For example:
-- |
-- |  sNs factorial sInit
-- |
-- | returns the final state:
-- |
-- |    s x = 1
-- |    s y = 6
-- |    s _ = 0
-- |
-- | Since a state is a function it cannot be printed thus you cannot
-- | add 'deriving Show' to the algebraic data type 'Config'.
-- | The goal of this exercise is to define a number of functions to
-- | "show" a state thus you can inspect the final state computed by the
-- | natural semantics of WHILE.

-- | Exercise 1.1
-- | Define a function 'showState' that given a state 's' and a list
-- | of variables 'vs' returns a list of strings showing the bindings
-- | of the variables mentioned in 'vs'. For example, for the state
-- | 's' above we get:
-- |
-- |    showState s ["x"] = ["x -> 1"]
-- |    showState s ["y"] = ["y -> 6"]
-- |    showState s ["x", "y"] = ["x -> 1", "y -> 6"]
-- |    showState s ["y", "z", "x"] = ["y -> 6", "z -> 0", "x -> 1"]

showState :: State -> [Var] -> [String]
showState s [] = []
showState s (x:xs) = (toString x) : (showState s xs)
  where
    toString x = x ++ " -> " ++ show (s x)

-- | Test your function with HUnit.


-- | Exercise 1.2
-- | Define a function 'fvStm' that returns the free variables of a WHILE
-- | statement. For example:
-- |
-- | fvStm factorial = ["y","x"]
-- |
-- | Note: the order of appearance is not relevant, but there should not be
-- | duplicates.

fvStm :: Stm -> [Var]
fvStm (Ass v a)            = nub $ [v] ++ fvAexp a
fvStm Skip                 = []
fvStm (Comp st1 st2)       = nub $ fvStm st1 ++ fvStm st2
fvStm (If b st1 st2)       = nub $ fvBexp b ++ fvStm st1 ++ fvStm st2
fvStm (While b st)         = nub $ fvBexp b ++ fvStm st
fvStm (Reptil b st)        = nub $ fvBexp b ++ fvStm st
fvStm (For var a1 a2 st)   = nub $ [var] ++ fvAexp a1 ++ fvAexp a2 ++ fvStm st
fvStm (For2 st1 b st2 st3) = nub $ fvStm st1 ++ fvBexp b ++ fvStm st2 ++ fvStm st3

-- | Test your function with HUnit. Beware the order or appearance.


-- | Exercise 1.3
-- | Define a function 'showFinalState' that given a WHILE statement and a
-- | initial state returns a list of strings with the bindings of
-- | the free variables of the statement in the final state. For
-- | example:
-- |
-- |  showFinalState factorial sInit = ["y->6","x->1"]

showFinalState :: Stm -> State -> [String]
showFinalState st s = showState (sNs st s) (fvStm st)

-- | Test your function with HUnit. Beware the order or appearance.


-- |----------------------------------------------------------------------
-- | Exercise 2
-- |----------------------------------------------------------------------
-- | Write a program in WHILE to compute z = x^y and check it by obtaining a
-- | number of final states.

sTest1 :: State
sTest1 "x" = 2
sTest1 "y" = 88
sTest1 _   = 0

power :: Stm
-- WHILE statement to compute z = x^y
power = Comp (Ass "z" (N 1))
             (While (Le (N 1) (V "y"))
                (Comp (Ass "z" (Mult (V "x") (V "z")))
                      (Ass "y" (Sub (V "y") (N 1)))))

-- | Test your function with HUnit. Inspect the final states of at least
-- | four different executions.


-- |----------------------------------------------------------------------
-- | Exercise 3
-- |----------------------------------------------------------------------
-- | The WHILE language can be extended with a 'repeat S until b' construct.

-- | Exercise 3.1
-- | Define the natural semantics of this new construct. You are not allowed
-- | to rely on the 'while b do S' statement.

{- Formal definition of 'repeat S until b'

                      <S,s> -> s', <repeat S until b, s'> -> s'' 
        [repeat-ff]  --------------------------------------------   if B[b]s = ff
                             <repeat S until b, s> -> s''


                                      <S,s> -> s'
        [repeat-tt]  --------------------------------------------  if B[b]s = tt
                             <repeat S until b, s> -> s' 

-}

-- | Extend  the definitions of  data type 'Stm' (in  module While.hs)
-- |  and  'nsStm'  (in  module NaturalSemantics.hs)  to  include  the
-- | 'repeat  S until b' construct.  Write a couple of  WHILE programs
-- | that use the 'repeat' statement and test your functions with HUnit.

sTest2 :: State
sTest2 "x" = 10
sTest2 _   = 0

-- copmute the xth triangular number
triangularNumber :: Stm
triangularNumber = Comp (Ass "z" (N 0))
                        (Comp (Ass "y" (N 0))
                              (Reptil (Eq (V "x") (V "y"))
                                 (Comp (Ass "z" (Add (V "z") (V "y")))
                                       (Ass "y" (Add (V "y") (N 1))))))

-- compute the xth square pyramidal number
squarePyramidalNumber :: Stm
squarePyramidalNumber = Comp (Ass "z" (N 0))
                             (Comp (Ass "y" (N 0))
                                   (Reptil (Eq (V "x") (V "y"))
                                      (Comp (Ass "z" (Add (V "z") (Mult (V "y") (V "y"))))
                                            (Ass "y" (Add (V "y") (N 1))))))

-- NB: The authors are aware of Faulhaber's formula and these WHILE programs are for testing purposes.

-- |----------------------------------------------------------------------
-- | Exercise 4
-- |----------------------------------------------------------------------
-- | The WHILE language can be extended with a 'for x:= a1 to a2 do S'
-- | construct.

-- | Exercise 4.1
-- | Define the natural semantics of this new construct. You are not allowed
-- | to rely on the 'while b do S' or the 'repeat S until b' statements.

{- Formal definition of 'for x:= a1 to a2 do S'

                     <x:=a1,s> -> s', <S,s'> -> s'', <for x:=a1+1 to a2 do S,s''> -> s'''
        [forin-ff]  ----------------------------------------------------------------------  if B[a1<=a2]s' = tt
                                       <for x:= a1 to a2 do S,s> -> s'''

        [forin-tt]  ----------------------------------  if B[a1<=a2]s' = ff
                     <for x:= a1 to a2 do S,s> -> s
-}

-- | Extend  the definitions of  data type 'Stm' (in  module While.hs)
-- | and  'nsStm'  (in  module NaturalSemantics.hs)  to  include  the
-- | 'for x:= a1 to a2 do S' construct.  Write a couple of  WHILE programs
-- | that use the 'for' statement and test your functions with HUnit.

sTest3 :: State
sTest3 "x" = 1
sTest3 "y" = 900
sTest3 _   = 0

sTest4 :: State
sTest4 "x" = 2
sTest4 "y" = 8
sTest4 _   = 0

-- compute the sum of all the integers between x and y, both inclusive
summation :: Stm
summation = Comp (Ass "z" (N 0))
                 (For "x"
                      (V "x")
                      (V "y")
                      (Ass "z" (Add (V "z") (V "x"))))
-- compute x^y
power2 :: Stm
power2 = Comp (Ass "z" (N 1))
              (For "i"
                   (N 1)
                   (V "y")
                   (Ass "z" (Mult (V "z") (V "x"))))

nestedFor :: Stm
nestedFor = For "i"
                (N 1)
                (N 10)
                (For "i"
                     (N 20)
                     (N 30)
                     (Ass "j" (Add (V "j") (V "i")))
                )

negativeFor :: Stm
negativeFor = For "i" (N 10) (N 1) Skip

manipulatedIterator :: Stm
manipulatedIterator = For "i" (N 1) (N 10)
                          (Comp (Ass "i" (Sub (V "i") (N 1)))
                                (Ass "j" (Add (V "j") (V "i"))))
                          

-- |----------------------------------------------------------------------
-- | Exercise 5
-- |----------------------------------------------------------------------

-- | Define the semantics of arithmetic expressions (Aexp) by means of
-- | natural semantics. To that end, define an algebraic datatype 'ConfigAexp'
-- | to represent the configurations, and a function 'nsAexp' to represent
-- | the transition relation.

-- representation of configurations for Aexp, (replace TODO by appropriate
-- data definition)

data ConfigAExp = InterA Aexp State -- <A, s>
                | FinalA Z          -- z

-- representation of the transition relation <A, s> -> z

nsAexp :: ConfigAExp -> ConfigAExp
-- N n
nsAexp (InterA (N n) s)        = FinalA n
-- V x
nsAexp (InterA (V x) s)        = FinalA $ s x
-- Add a1 a2
nsAexp (InterA (Add a1 a2) s)  = FinalA $ z1 + z2
  where
    FinalA z1 = nsAexp (InterA a1 s)
    FinalA z2 = nsAexp (InterA a2 s)
-- Mult a1 a2
nsAexp (InterA (Mult a1 a2) s) = FinalA $ z1 * z2
  where
    FinalA z1 = nsAexp (InterA a1 s)
    FinalA z2 = nsAexp (InterA a2 s)
-- Sub a1 a2
nsAexp (InterA (Sub a1 a2) s)  = FinalA $ z1 - z2
  where
    FinalA z1 = nsAexp (InterA a1 s)
    FinalA z2 = nsAexp (InterA a2 s)

showFinalAexp :: ConfigAExp -> Z
showFinalAexp (FinalA z) = z
showFinalAexp _          = error "showFinalAexp on a non-final config"

-- | Test your function with HUnit. Inspect the final states of at least
-- | four different evaluations.


-- |----------------------------------------------------------------------
-- | Exercise 6
-- |----------------------------------------------------------------------

-- | Given the algebraic data type 'DerivTree' to represent derivation trees
-- | of the natural semantics:

data Transition = Config :-->: State

data DerivTree = AssNS     Transition
               | SkipNS    Transition
               | CompNS    Transition DerivTree DerivTree
               | IfTTNS    Transition DerivTree
               | IfFFNS    Transition DerivTree
               | WhileTTNS Transition DerivTree DerivTree
               | WhileFFNS Transition

-- | and the function 'getFinalState' to access the final state of the root
-- | of a derivation tree:

getFinalState :: DerivTree -> State
getFinalState (AssNS  (_ :-->: s))         = s
getFinalState (SkipNS (_ :-->: s))         = s
getFinalState (CompNS (_ :-->: s) _ _ )    = s
getFinalState (IfTTNS (_ :-->: s) _ )      = s
getFinalState (IfFFNS (_ :-->: s) _ )      = s
getFinalState (WhileTTNS (_ :-->: s) _ _ ) = s
getFinalState (WhileFFNS (_ :-->: s))      = s

-- | Define a function 'nsDeriv' that given a WHILE statement 'st' and an
-- | initial state 's' returns corresponding derivation tree.

nsDeriv :: Stm -> State -> DerivTree
nsDeriv st@(Ass v a) s0       = AssNS (Inter st s0 :-->: sNs st s0)
nsDeriv st@(Skip) s0          = SkipNS (Inter st s0 :-->: s0)
nsDeriv st@(Comp st1 st2) s0  = CompNS (Inter st s0 :-->: sNs st s0) (nsDeriv st1 s0) (nsDeriv st1 s1)
  where s1 = sNs st1 s0
nsDeriv st@(If b st1 st2) s0 
  | bVal b s0 == True  = IfTTNS (Inter st s0 :-->: sNs st s0) (nsDeriv st1 s0)
  | otherwise       = IfFFNS (Inter st s0 :-->: sNs st s0) (nsDeriv st2 s0)
nsDeriv st@(While b stm) s0
  | bVal b s0 == True  = WhileTTNS (Inter st s0 :-->: sNs st s0) (nsDeriv stm s0) (nsDeriv st s1)
  | otherwise       = WhileFFNS (Inter st s0 :-->: sNs st s0)
  where s1 = sNs st s0
