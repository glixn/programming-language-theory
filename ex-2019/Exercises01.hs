{-|

Programming Languages
Fall 2019

Implementation in Haskell of the concepts covered in Chapter 1 of
Nielson & Nielson, Semantics with Applications

Author: Alberto Carmona, Sergio Cayuela

-}

module Exercises01 where

import           Test.HUnit hiding (State)
import           Data.List
import           While

-- |----------------------------------------------------------------------
-- | Expressions to test with
-- |----------------------------------------------------------------------

-- 0011010 (trailing zeroes to test normalization with)
twentysix :: Bin
twentysix = B (B (B (B (B (B (MSB O) O) I) I) O) I) O

s1 :: State
s1 "x" =  3
s1 "y" =  4
s1 "z" =  5
s1 _   =  0

-- ((3+4)*(5+x))-(y+z)
aexp1 :: Aexp
aexp1 = Sub (Mult (Add (N 3) (N 4)) (Add (N 5) (V "x"))) (Add (V "y") (V "z"))

-- x*((3+4)*(5+y))
aexp2 :: Aexp
aexp2 = Mult (V "x") (Mult (Add (N 3) (N 4)) (Add (N 5) (V "y")))

-- ¬( ((8-5)<=x) && (1==(4-3)) )
bexp1 :: Bexp
bexp1 = Neg (And (Le (Sub (N 8) (N 5)) (V "x")) (Eq (N 1) (Sub (N 4) (N 3))))

-- |----------------------------------------------------------------------
-- | Exercise 1
-- |----------------------------------------------------------------------
-- | Given the algebraic data type 'Bin' for the binary numerals:

data Bit = O
         | I
         deriving (Eq, Show)

data Bin = MSB Bit
         | B Bin Bit
         deriving (Eq, Show)

-- | and the following values of type 'Bin':

zero :: Bin
zero = MSB O

one :: Bin
one = MSB I
three :: Bin
three = B (B (MSB O) I) I

six :: Bin
six = B (B (MSB I) I) O

-- | define a semantic function 'binVal' that associates
-- | a number (in the decimal system) to each binary numeral.

binVal :: Bin -> Z
binVal (MSB O) = 0
binVal (MSB I) = 1
binVal (B n O) = 2*(binVal n)
binVal (B n I) = 2*(binVal n)+1

-- | Test your function with HUnit.
-- runTestTT testBinVal

testBinVal :: Test
testBinVal = test ["value of zero"  ~: 0 ~=? binVal zero,
                   "value of one"   ~: 1 ~=? binVal one,
                   "value of three" ~: 3 ~=? binVal three,
                   "value of six"   ~: 6 ~=? binVal six]

-- | Define a function 'foldBin' to fold a value of type 'Bin'

foldBin :: (b -> Bin -> b) -> (Bin -> b) -> Bin -> b
foldBin recur base n = fold n
  where
    fold (MSB bit) = base (MSB bit)
    fold (B n bit) = recur (fold n) (MSB bit) 

-- | and use 'foldBin' to define a function 'binVal''  equivalent to 'binVal'.

binVal' :: Bin -> Integer
binVal' = foldBin recur base
  where
    base (MSB O) = 0
    base (MSB I) = 1
    recur n bit  = 2 * n + (base bit)

-- | Test your function with HUnit.

testBinVal' :: Test
testBinVal' = test ["value of zero"  ~: 0 ~=? binVal' zero,
                    "value of one"   ~: 1 ~=? binVal' one,
                    "value of three" ~: 3 ~=? binVal' three,
                    "value of six"   ~: 6 ~=? binVal' six]

-- | Define a function 'hammingWeight' that returns the number of ones occurring
-- | in a binary numeral.

hammingWeight :: Bin -> Integer
hammingWeight (MSB O) = 0
hammingWeight (MSB I) = 1
hammingWeight (B n O) = 0 + hammingWeight n
hammingWeight (B n I) = 1 + hammingWeight n

-- | and use 'foldBin' to define a function 'hammingWeight''  equivalent to 'hammingWeight'.

hammingWeight' :: Bin -> Integer
hammingWeight' = foldBin recur base
  where
    base (MSB O) = 0 
    base (MSB I) = 1 
    recur n bit  = n + (base bit)

-- | Test your functions with HUnit.

testHammingWeight :: Test
testHammingWeight = test ["hamming weight of zero"  ~: 0 ~=? hammingWeight zero,
                          "hamming weight of one"   ~: 1 ~=? hammingWeight one,
                          "hamming weight of three" ~: 2 ~=? hammingWeight three,
                          "hamming weight of six"   ~: 2 ~=? hammingWeight six]

testHammingWeight' :: Test
testHammingWeight' = test ["hamming weight of zero"  ~: 0 ~=? hammingWeight' zero,
                           "hamming weight of one"   ~: 1 ~=? hammingWeight' one,
                           "hamming weight of three" ~: 2 ~=? hammingWeight' three,
                           "hamming weight of six"   ~: 2 ~=? hammingWeight' six]

-- | Define a function 'complement' that returns the complement of a binary numeral

complement :: Bin -> Bin
complement (MSB O) = MSB I
complement (MSB I) = MSB O
complement (B n O) = ( B (complement n) I )
complement (B n I) = ( B (complement n) O )

-- | and use 'foldBin' to define a function 'complement''  equivalent to 'complement'.

complement' :: Bin -> Bin
complement' = foldBin recur base
  where 
    base (MSB O)   = MSB I
    base (MSB I)   = MSB O
    recur n (MSB O) = (B n I)
    recur n (MSB I) = (B n O)

-- | Test your functions with HUnit.

-- todo

-- | Define a function 'normalize' that given a binary numeral trims leading zeroes.

normalize :: Bin -> Bin
normalize (MSB bit) = MSB bit
normalize (B n bit) | (normalize n) == (MSB O)  = (MSB bit)
                    | otherwise                 = (B (normalize n) bit) 

-- | and use 'foldBin' to define a function 'normalize''  equivalent to 'normalize'.

normalize' :: Bin -> Bin
normalize' = foldBin recur id
  where
    recur n (MSB bit) | n == (MSB O) = (MSB bit)
                      | otherwise    = B n bit

-- | Test your functions with HUnit.

-- todo

-- |----------------------------------------------------------------------
-- | Exercise 2
-- |----------------------------------------------------------------------
-- | Define the function 'fvAexp' that computes the set of free variables
-- | occurring in an arithmetic expression. Ensure that each free variable
-- | occurs once in the resulting list.

fvAexp :: Aexp -> [Var]
fvAexp (N _)        = []
fvAexp (V v)        = [v]
fvAexp (Add a1 a2)  = nub ((fvAexp a1) ++ (fvAexp a2))
fvAexp (Mult a1 a2) = nub ((fvAexp a1) ++ (fvAexp a2))
fvAexp (Sub a1 a2)  = nub ((fvAexp a1) ++ (fvAexp a2))

-- | Test your function with HUnit.

-- todo

-- | Define the function 'fvBexp' that computes the set of free variables
-- | occurring in a Boolean expression.

fvBexp :: Bexp -> [Var]
fvBexp (Eq a1 a2)  = nub ((fvAexp a1) ++ (fvAexp a2))
fvBexp (Le a1 a2)  = nub ((fvAexp a1) ++ (fvAexp a2))
fvBexp (Neg b)     = fvBexp b
fvBexp (And b1 b2) = nub ((fvBexp b1) ++ (fvBexp b2))
fvBexp _           = []

-- | Test your function with HUnit.

-- todo

-- |----------------------------------------------------------------------
-- | Exercise 3
-- |----------------------------------------------------------------------
-- | Given the algebraic data type 'Subst' for representing substitutions:

data Subst = Var :->: Aexp

-- | define a function 'substAexp' that takes an arithmetic expression
-- | 'a' and a substitution 'y:->:a0' and returns the substitution a [y:->:a0];
-- | i.e., replaces every occurrence of 'y' in 'a' by 'a0'.

substAexp :: Aexp -> Subst -> Aexp
substAexp (Add a1 a2) sub  = Add (substAexp a1 sub) (substAexp a2 sub)
substAexp (Mult a1 a2) sub = Mult (substAexp a1 sub) (substAexp a2 sub)
substAexp (Sub a1 a2) sub  = Sub (substAexp a1 sub) (substAexp a2 sub)
substAexp (N n) _        = N n
substAexp (V v1) (v2 :->: a)
    | v1 == v2    = a
    | otherwise = (V v1)

-- | Test your function with HUnit.

-- todo

-- | Define a function 'substBexp' that implements substitution for
-- | Boolean expressions.

substBexp :: Bexp -> Subst -> Bexp
substBexp (Eq a1 a2) sub = Eq (substAexp a1 sub) (substAexp a2 sub) 
substBexp (Le a1 a2) sub = Le (substAexp a1 sub) (substAexp a2 sub) 
substBexp (Neg b) sub = Neg (substBexp b sub)
substBexp (And b1 b2) sub = And (substBexp b1 sub) (substBexp b2 sub) 
substBexp b _ = b

-- | Test your function with HUnit.

-- todo

-- |----------------------------------------------------------------------
-- | Exercise 4
-- |----------------------------------------------------------------------
-- | Given the algebraic data type 'Update' for state updates:

data Update = Var :=>: Z

-- | define a function 'update' that takes a state 's' and an update 'x :=> v'
-- | and returns the updated state 's [x :=> v]'

update :: State -> Update -> State
update s (v:=>:n) = updatedFun
  where
    updatedFun arg | arg == v  = n
                   | otherwise = s arg


-- | Test your function with HUnit.

-- todo

-- | Define a function 'updates' that takes a state 's' and a list of updates
-- | 'us' and returns the updated states resulting from applying the updates
-- | in 'us' from head to tail. For example:
-- |
-- |    updates s ["x" :=>: 1, "y" :=>: 2, "x" :=>: 3]
-- |
-- | returns a state that binds "x" to 3 (the most recent update for "x").

updates :: State ->  [Update] -> State
updates s [] = s
updates s (x:xs) = updates (update s x) xs

-- |----------------------------------------------------------------------
-- | Exercise 5
-- |----------------------------------------------------------------------
-- | Define a function 'foldAexp' to fold an arithmetic expression

foldAexp :: (Z -> a) -> (Var -> a) -> (a -> a -> a) -> (a -> a -> a) -> (a -> a -> a) -> Aexp -> a
foldAexp num var add mul sub (N n)        = num n
foldAexp num var add mul sub (V x)        = var x
foldAexp num var add mul sub (Add a1 a2)  = add (foldAexp num var add mul sub a1) (foldAexp num var add mul sub a2)
foldAexp num var add mul sub (Mult a1 a2) = mul (foldAexp num var add mul sub a1) (foldAexp num var add mul sub a2)
foldAexp num var add mul sub (Sub a1 a2)  = sub (foldAexp num var add mul sub a1) (foldAexp num var add mul sub a2)

-- | Use 'foldAexp' to define the functions 'aVal'', 'fvAexp'', and 'substAexp''
-- | and test your definitions with HUnit.

aVal' :: Aexp -> State -> Z
aVal' a state = foldAexp id state (+) (*) (-) a

fvAexp' :: Aexp -> [Var]
fvAexp' = foldAexp (const []) (\x -> [x]) f f f
  where
    f a1 a2 = nub (a1 ++ a2)

substAexp' :: Aexp -> Subst -> Aexp
substAexp' a1 (v :->: a2) = foldAexp N (f v a2) Add Mult Sub a1
  where
    f x y z | x == z = y
            | otherwise = (V x)

-- | Define a function 'foldBexp' to fold a Boolean expression and use it
-- | to define the functions 'bVal'', 'fvBexp'', and 'substAexp''. Test
-- | your definitions with HUnit.

foldBexp :: (Aexp -> a) -> (Bexp -> b) -> (a -> a -> b) -> (a -> a -> b) -> (b -> b) -> (b -> b -> b) -> Bexp -> b
foldBexp afold base eql leq neg and TRUE        = base TRUE
foldBexp afold base eql leq neg and FALSE       = base FALSE
foldBexp afold base eql leq neg and (Eq a1 a2)  = eql (afold a1) (afold a2)
foldBexp afold base eql leq neg and (Le a1 a2)  = leq (afold a1) (afold a2)
foldBexp afold base eql leq neg and (Neg b)     = neg (foldBexp afold base eql leq neg and b)
foldBexp afold base eql leq neg and (And b1 b2) = and (foldBexp afold base eql leq neg and b1) (foldBexp afold base eql leq neg and b2)

bVal' :: Bexp -> State -> Bool
bVal' b state = foldBexp ((\state a -> aVal a state) state) f (==) (<=) not (&&) b
  where 
    f TRUE = True
    f FALSE = False

fvBexp' :: Bexp -> [Var]
fvBexp' = foldBexp fvAexp (const []) f f id f
  where
    f x y = nub (x ++ y)

substBexp' :: Bexp -> Subst -> Bexp
substBexp' b sub = foldBexp ((\sub a -> substAexp' a sub) sub) id Eq Le Neg And b
