{-|

Programming Languages
Examen 2019

-}

module Examen2019 where

import           While
import           NaturalSemantics
import           Exercises01
import           Exercises02


-- |----------------------------------------------------------------------
-- | Ejercicio 3 - NS
-- |----------------------------------------------------------------------

-- Implementación en NS y SOS de la construcción iterativa for similar al de Java:
-- for S1 b S2 S3 donde b es una guarda precomprobada, S1 se ejecuta solo al inicio, 
-- S2 se ejecuta tras cada iteración y S3 es el cuerpo del bucle.

-- Código añadido en NaturalSemantics.hs:
-- nsStm :: Config -> Config
-- nsStm (Inter (For2 st1 b st2 st3) s) = Final s'
--     where
--       s'   = sNs' (Comp st1 (While b (Comp st3 st2))) s
-- nsStm x                              = nsStm x

-- Código añadido en NaturalSemantics.hs:
-- fvStm (For2 st1 b st2 st3) = nub $ fvStm st1 ++ fvBexp b ++ fvStm st2 ++ fvStm st3

-- Programa de prueba
-- Calcula el factorial de 5
forTest :: Stm
forTest = Comp (Ass "y" (N 1))
               (For2 (Ass "x" (N 1))
                   (Le (V "x") (N 5)) 
                   (Ass "x" (Add (V "x") (N 1)))
                   (Ass "y" (Mult (V "y") (V "x"))))

sFor :: [String]
sFor = showFinalState forTest sInit
