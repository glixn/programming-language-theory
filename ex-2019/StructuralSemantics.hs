{-|

Programming Languages
Fall 2019

Implementation in Haskell of the Structural Operational Semantics
described in Chapter 2 of Nielson & Nielson, Semantics with Applications

Author: Alberto Carmona López, Sergio Cayuela Domínguez

-}

module StructuralSemantics where

import           ExWhile

-- representation of configurations for While

data Config = Inter Stm State  -- <S, s>
            | Final State      -- s
            | Stuck Stm State  -- <S, s> / La primera sentencia es la que provoca el abort

isFinal :: Config -> Bool
isFinal (Final s)    = True
isFinal _            = False

isStuck :: Config -> Bool
isStuck (Stuck _ _)  = True
isStuck _            = False

isInter :: Config -> Bool
isInter (Inter _ _)  = True
isInter _            = False

-- representation of the transition relation <S, s> -> s'

sosStm :: Config -> Config

-- x := a

sosStm (Inter (Ass x a) s) = Final (update s x (aVal a s))
  where
    update s x v y = if x == y then v else s y

-- skip

sosStm (Inter Skip s) = Final s

-- s1; s2

sosStm (Inter st@(Comp st1 st2) s)
  | isInter next     = Inter (Comp st1' st2) s'
  | isStuck next     = Stuck st s
  | otherwise        = Inter st2 s''
    where 
      next           = sosStm (Inter st1 s)
      following      = sosStm (Inter st2 s'')
      Inter st1' s'  = sosStm (Inter st1 s)
      Final s''      = sosStm (Inter st1 s)

-- if b then s1 else s2

sosStm (Inter (If b ss1 ss2) s)
  | bVal b s  = Inter ss1 s
  | otherwise = Inter ss2 s

-- while b do s
sosStm (Inter while@(While b ss) s) = Inter unfold s
    where
        unfold = If b (Comp ss while) Skip

-- repeat s until b
sosStm (Inter repeat@(Repeat ss b) s) = Inter unfold s
    where
        unfold = If b ss (Comp ss repeat) 

-- abort
sosStm (Inter Abort s) = (Stuck Abort s)

-- for (s1; b; s2) s3
sosStm (Inter (For s1 b s2 s3) s) = Inter (Comp s1 (While b (Comp s3 s2))) s
