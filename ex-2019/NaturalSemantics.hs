{-|

Programming Languages
Fall 2019

Implementation in Haskell of the Natural Semantics described in Chapter 2 of
Nielson & Nielson, Semantics with Applications

Author: Alberto Carmona López, Sergio Cayuela Domínguez

-}

module NaturalSemantics where

import           While
import           Exercises01      (Update (..), fvAexp, fvBexp, update)

-- representation of configurations for While

data Config = Inter Stm State  -- <S, s>
            | Final State      -- s

testStm :: Stm
testStm = Comp (Ass "x" (N 1)) (Ass "y" (N 2))

-- representation of the transition relation <S, s> -> s'

nsStm :: Config -> Config

-- x := a

nsStm (Inter (Ass x a) s)      = Final $ update s (x :=>: (aVal a s))

-- skip

nsStm (Inter Skip s)           = Final s

-- s1; s2

nsStm (Inter (Comp st1 st2) s) = Final s''
  where
    s'  = sNs st1 s
    s'' = sNs st2 s'

-- if b then s1 else s2

nsStm (Inter (If b st1 st2) s) 
  | (bVal b s) == True  = Final (sNs st1 s) -- B[b]s = tt
  | (bVal b s) == False = Final (sNs st2 s) -- B[b]s = ff

-- while b do s

nsStm (Inter (While b st) s)
  | (bVal b s) == True  = Final s'' -- B[b]s = tt
  | (bVal b s) == False = Final s   -- B[b]s = ff
    where 
      s'  = sNs st s
      s'' = sNs (While b st) s'

-- repeat s until b

nsStm (Inter (Reptil b st) s)
  | (bVal b s) == True  = Final s'
  | (bVal b s) == False = Final s''
    where
      s'  = sNs st s
      s'' = sNs (Reptil b st) s'

-- for x:=a1 to a2 do S

nsStm (Inter (For var a1 a2 st) s)
  | (bVal (Le a1 a2) s') == False = Final s
  | (bVal (Le a1 a2) s') == True  = Final s'''
    where
      s'   = sNs (Ass var a1) s
      s''  = sNs st s'
      s''' = sNs (For var (Add a1 (N 1)) a2 st) s''
--      nextInt = (N (aVal (Add a1 (N 1)) s''))

-- for (s1; b; s2) s3
nsStm (Inter (For2 st1 b st2 st3) s) = Final s'
    where
      s'   = sNs (Comp st1 (While b (Comp st3 st2))) s

-- semantic function for natural semantics
sNs :: Stm -> State -> State
sNs ss s = s'
  where Final s' = nsStm (Inter ss s)

-- Example C.1
sFac :: State
sFac = sNs factorial sInit
-- End Example C.1
