{-|

Programming Languages
Examen 2019

-}

module Examen2019SOS where

import           ExWhile
import           StructuralSemantics
import           Exercises03


-- |----------------------------------------------------------------------
-- | Ejercicio 3 - SOS
-- |----------------------------------------------------------------------

-- Implementación en NS y SOS de la construcción iterativa for similar al de Java:
-- for S1 b S2 S3 donde b es una guarda precomprobada, S1 se ejecuta solo al inicio, 
-- S2 se ejecuta tras cada iteración y S3 es el cuerpo del bucle.

-- Código añadido en StructuralSemantics.hs
-- for (s1; b; s2) s3
-- sosStm (Inter (For s1 b s2 s3) s) = Inter (Comp s1 (While b (Comp s3 s2))) s

-- Programa de prueba
-- Calcula el factorial
forTest :: Stm
forTest = Comp (Ass "y" (N 1))
               (For (Ass "x" (N 1))
                   (Le (V "x") (N 5)) 
                   (Ass "x" (Add (V "x") (N 1)))
                   (Ass "y" (Mult (V "y") (V "x"))))

forSeq :: DerivSeq
forSeq = derivSeq forTest sInit

showForSeq :: IO()
showForSeq = putStrLn $ showDerivSeq ["x", "y"] forSeq
